package com.example.testmemcache;

import com.example.testmemcache.lfu.FastLFUMemCache;
import com.example.testmemcache.lru.LinkedHashMapLRUMemCache;

import java.util.Optional;

public class ConfigurableMemCache<K, V> implements MemCache<K, V> {

    public enum EvictionStrategy {
        LRU,
        LFU
    }

    private MemCache<K, V> backend;

    public ConfigurableMemCache(int maxSize, EvictionStrategy evictionStrategy) {
        if (maxSize <= 0) throw new IllegalArgumentException("Cache size must be greater than zero");

        switch (evictionStrategy) {
            case LRU:
                backend = new LinkedHashMapLRUMemCache<>(maxSize);
                break;
            case LFU:
                backend = new FastLFUMemCache<>(maxSize);
                break;
            default:
                throw new IllegalArgumentException("Unknown eviction strategy");
        }
    }

    @Override
    public void put(K key, V value) {
        backend.put(key, value);
    }

    @Override
    public Optional<V> get(K key) {
        return backend.get(key);
    }

    @Override
    public void remove(K key) {
        backend.remove(key);
    }

    @Override
    public void clear() {
        backend.clear();
    }
}
