package com.example.testmemcache;

import java.util.Optional;

public interface MemCache<K, V> {
    void put(K key, V value);

    Optional<V> get(K key);

    void remove(K key);

    void clear();
}
