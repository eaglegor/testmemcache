package com.example.testmemcache.util;

public class IntrusiveList<V extends IntrusiveList.IntrusiveListNode> {

    public static class IntrusiveListNode {

        private IntrusiveList.IntrusiveListNode next = this;
        private IntrusiveList.IntrusiveListNode prev = this;

        public IntrusiveList.IntrusiveListNode getNext() {
            return next;
        }

        public IntrusiveList.IntrusiveListNode getPrev() {
            return prev;
        }

        public void setNext(IntrusiveList.IntrusiveListNode next) {
            this.next = next;
        }

        public void setPrev(IntrusiveList.IntrusiveListNode prev) {
            this.prev = prev;
        }
    }

    private IntrusiveListNode dummyNode = new IntrusiveListNode();
    private int size = 0;

    public void add(V value) {
        V lastNode = getLast();
        if (lastNode == null) {
            dummyNode.prev = value;
            dummyNode.next = value;
            value.setPrev(dummyNode);
            value.setNext(dummyNode);
            size = 1;
        } else {
            add(lastNode.getNext(), value);
        }
    }

    public void add(IntrusiveListNode pos, V value) {
        IntrusiveListNode prevNode = pos.getPrev();
        IntrusiveListNode nextNode = pos;

        prevNode.setNext(value);
        value.setPrev(prevNode);
        nextNode.setPrev(value);
        value.setNext(nextNode);

        ++size;
    }

    public void addFirst(V value) {
        if (size >= 1) add(getFirst(), value);
        else add(value);
    }

    public V getFirst() {
        if (dummyNode.next == dummyNode) return null;
        return (V) dummyNode.next;
    }

    public V getLast() {
        if (dummyNode.prev == dummyNode) return null;
        return (V) dummyNode.prev;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void remove(V node) {
        if (node == null) return;
        IntrusiveListNode prevNode = node.getPrev();
        IntrusiveListNode nextNode = node.getNext();

        prevNode.setNext(nextNode);
        nextNode.setPrev(prevNode);

        node.setPrev(null);
        node.setNext(null);

        --size;
    }

    public int size() {
        return size;
    }

    public void clear() {
        while (size > 0) remove(getFirst());
    }

}
