package com.example.testmemcache.lru;

import com.example.testmemcache.MemCache;

import java.util.Optional;

public class LinkedHashMapLRUMemCache<K, V> implements MemCache<K, V> {

    private LimitedLinkedHashMap<K, V> storage;

    public LinkedHashMapLRUMemCache(int maxSize) {
        if (maxSize <= 0) throw new IllegalArgumentException("Cache size must be greater than zero");
        storage = new LimitedLinkedHashMap<>(maxSize);
    }

    @Override
    public void put(K key, V value) {
        storage.put(key, value);
    }

    @Override
    public Optional<V> get(K key) {
        return Optional.ofNullable(storage.get(key));
    }

    @Override
    public void remove(K key) {
        storage.remove(key);
    }

    @Override
    public void clear() {
        storage.clear();
    }
}
