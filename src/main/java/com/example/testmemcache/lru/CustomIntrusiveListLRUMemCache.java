package com.example.testmemcache.lru;

import com.example.testmemcache.MemCache;
import com.example.testmemcache.util.IntrusiveList;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CustomIntrusiveListLRUMemCache<K, V> implements MemCache<K, V> {

    private static class LRUCacheEntry<K, V> extends IntrusiveList.IntrusiveListNode {
        public LRUCacheEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K key;
        public V value;
    }

    private IntrusiveList<LRUCacheEntry<K, V>> storage = new IntrusiveList<>();
    private Map<K, LRUCacheEntry<K, V>> mapping = new HashMap<>();

    private int maxSize;

    public CustomIntrusiveListLRUMemCache(int maxSize) {
        if (maxSize <= 0) throw new IllegalArgumentException("Cache size must be greater than zero");
        this.maxSize = maxSize;
    }

    @Override
    public void put(K key, V value) {
        LRUCacheEntry<K, V> entry = mapping.get(key);
        if (entry != null) {
            entry.value = value;
            use(entry);
            return;
        }
        if (storage.size() == maxSize) {
            evict();
        }
        entry = new LRUCacheEntry<>(key, value);
        storage.addFirst(entry);
        mapping.put(key, entry);
    }

    @Override
    public Optional<V> get(K key) {
        LRUCacheEntry<K, V> entry = mapping.get(key);
        if (entry == null) return Optional.empty();
        else {
            use(entry);
            return Optional.of(entry.value);
        }
    }

    @Override
    public void remove(K key) {
        LRUCacheEntry<K, V> entry = mapping.get(key);
        if (entry == null) return;
        mapping.remove(key);
        storage.remove(entry);
    }

    @Override
    public void clear() {
        storage.clear();
        mapping.clear();
    }

    private void use(LRUCacheEntry<K, V> entry) {
        if (storage.size() == 1) return;
        storage.remove(entry);
        storage.addFirst(entry);
    }

    private void evict() {
        if (storage.isEmpty()) {
            throw new IllegalStateException("Eviction requested but no element is eligible for eviction");
        }
        mapping.remove(storage.getLast().key);
        storage.remove(storage.getLast());
    }
}
