package com.example.testmemcache.lru;

import java.util.LinkedHashMap;
import java.util.Map;

public class LimitedLinkedHashMap<K, V> extends LinkedHashMap<K, V> {

    private int maxSize;

    // I'd rather take these from HashMap constants but unfortunately they are inaccessible there
    private static int DEFAULT_INITIAL_CAPACITY = 16;
    private static float DEFAULT_LOAD_FACTOR = 0.75f;

    public LimitedLinkedHashMap(int maxSize) {
        super(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR, true);
        this.maxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > maxSize;
    }
}
