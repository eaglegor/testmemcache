package com.example.testmemcache.lfu;

class LFUMemCacheEntry<K, V> implements Comparable<LFUMemCacheEntry<K, V>> {
    public K key;
    public V value;
    public int usages;

    public LFUMemCacheEntry(K key, V value, int usages) {
        this.key = key;
        this.value = value;
        this.usages = usages;
    }

    @Override
    public int compareTo(LFUMemCacheEntry<K, V> o) {
        return Integer.compare(o.usages, usages);
    }
}
