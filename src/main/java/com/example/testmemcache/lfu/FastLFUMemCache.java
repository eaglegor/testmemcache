package com.example.testmemcache.lfu;

import com.example.testmemcache.MemCache;
import com.example.testmemcache.util.IntrusiveList;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;

// O(1) insertion
// O(1) retrieval
// O(1) removal
// O(1) eviction
public class FastLFUMemCache<K, V> implements MemCache<K, V> {

    private static class Bucket<K, V> extends IntrusiveList.IntrusiveListNode {
        public LinkedHashSet<BucketLFUMemCacheEntry<K, V>> data = new LinkedHashSet<>();

        int getUsagesCount() {
            if (data.isEmpty()) throw new IllegalStateException("Stale bucket found");

            return data.iterator().next().usages;
        }
    }

    private static class BucketLFUMemCacheEntry<K, V> extends LFUMemCacheEntry<K, V> {
        public Bucket bucket;

        public BucketLFUMemCacheEntry(K key, V value, int usages, Bucket<K, V> bucket) {
            super(key, value, usages);
            this.bucket = bucket;
        }
    }

    private Map<K, BucketLFUMemCacheEntry<K, V>> mapping = new HashMap<>();
    private IntrusiveList<Bucket<K, V>> storage = new IntrusiveList<>();
    private int maxSize;
    private int size = 0;

    public FastLFUMemCache(int maxSize) {
        if (maxSize <= 0) throw new IllegalArgumentException("Cache size must be greater than zero");
        this.maxSize = maxSize;
    }

    @Override
    public void put(K key, V value) {
        BucketLFUMemCacheEntry<K, V> entry = mapping.get(key);
        if (entry != null) {
            entry.value = value;
            use(entry);
            return;
        }
        if (size == maxSize) {
            evict();
        }

        if (storage.isEmpty() || storage.getFirst().getUsagesCount() > 1) {
            storage.addFirst(new Bucket<>());
        }
        Bucket<K, V> bucket = storage.getFirst();

        entry = new BucketLFUMemCacheEntry<>(key, value, 1, bucket);
        mapping.put(key, entry);
        bucket.data.add(entry);
        ++size;
    }

    @Override
    public Optional<V> get(K key) {
        BucketLFUMemCacheEntry<K, V> entry = mapping.get(key);
        if (entry == null) {
            return Optional.empty();
        }
        use(entry);
        return Optional.of(entry.value);
    }

    @Override
    public void remove(K key) {
        BucketLFUMemCacheEntry<K, V> entry = mapping.get(key);
        if (entry == null) return;
        Bucket bucket = entry.bucket;
        bucket.data.remove(entry);
        if (bucket.data.isEmpty()) storage.remove(bucket);
        mapping.remove(key);
    }

    @Override
    public void clear() {
        storage.clear();
        mapping.clear();
    }

    private void use(BucketLFUMemCacheEntry<K, V> entry) {
        Bucket<K, V> bucket = entry.bucket;
        Bucket<K, V> nextBucket;
        if (bucket == storage.getLast()) {
            nextBucket = new Bucket<>();
            storage.add(nextBucket);
        } else if (((Bucket<K, V>) bucket.getNext()).getUsagesCount() > bucket.getUsagesCount() + 1) {
            nextBucket = new Bucket<>();
            storage.add(bucket.getNext(), nextBucket);
        } else {
            nextBucket = (Bucket<K, V>) bucket.getNext();
        }
        bucket.data.remove(entry);
        nextBucket.data.add(entry);
        entry.bucket = nextBucket;
        ++entry.usages;
        if (bucket.data.isEmpty()) storage.remove(bucket);
    }

    private void evict() {
        if (storage.isEmpty()) {
            throw new IllegalStateException("Eviction requested but no element is eligible for eviction");
        }
        LFUMemCacheEntry<K, V> entry = storage.getFirst().data.iterator().next();
        mapping.remove(entry.key);
        Bucket<K, V> bucket = ((BucketLFUMemCacheEntry<K, V>) entry).bucket;
        bucket.data.remove(entry);
        if (bucket.data.isEmpty()) {
            storage.remove(bucket);
        }
        --size;
    }
}
