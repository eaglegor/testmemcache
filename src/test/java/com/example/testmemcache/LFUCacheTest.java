package com.example.testmemcache;

import com.example.testmemcache.lfu.FastLFUMemCache;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class LFUCacheTest {
    @DataProvider(name = "LFUCaches")
    Object[][] createLFUImplementations() {
        return new Object[][]
                {
                        {new FastLFUMemCache<Integer, Integer>(10)},
                        {new ConfigurableMemCache<Integer, Integer>(10, ConfigurableMemCache.EvictionStrategy.LFU)}
                };
    }


    @Test(dataProvider = "LFUCaches")
    public void testLeastUsedEntriesAreEvicted(MemCache<Integer, Integer> cache) {
        for (int i = 0; i < 10; ++i) {
            cache.put(i, i);
        }

        for (int j = 0; j < 5; ++j) {
            for (int i = 0; i < 10; i += 2) {
                if ((i / 2) % 2 == 0) cache.get(i);
                else cache.put(i, i);
            }
        }

        for (int i = 1; i < 10; i += 2) {
            if ((i / 2) % 2 == 0) cache.get(i);
            else cache.put(i, i);
        }

        for (int i = 10; i < 15; ++i) {
            cache.put(i, i);
            cache.get(i);
        }

        for (int i = 0; i < 10; i += 2) assertTrue(cache.get(i).isPresent());
        for (int i = 1; i < 10; i += 2) assertFalse(cache.get(i).isPresent());
        for (int i = 10; i < 15; ++i) assertTrue(cache.get(i).isPresent());
    }

    @Test(dataProvider = "LFUCaches")
    public void testCorrectValuesAreStored(MemCache<Integer, Integer> cache) {
        CacheTestUtil.testCorrectValuesAreStored(cache);
    }

    @Test(dataProvider = "LFUCaches")
    public void testClearWorks(MemCache<Integer, Integer> cache) {
        CacheTestUtil.testClearWorks(cache);
    }

    @Test(dataProvider = "LFUCaches")
    public void testRemovalWorks(MemCache<Integer, Integer> cache) {
        CacheTestUtil.testRemovalWorks(cache);
    }

    @Test(dataProvider = "LFUCaches")
    public void testNullValuesAreHandledProperly(MemCache<Integer, Integer> cache) {
        CacheTestUtil.testNullValuesAreHandledProperly(cache);
    }
}
