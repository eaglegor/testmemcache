package com.example.testmemcache;

import com.example.testmemcache.lru.CustomIntrusiveListLRUMemCache;
import com.example.testmemcache.lru.LinkedHashMapLRUMemCache;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class LRUCacheTest {
    @DataProvider(name = "LRUCaches")
    Object[][] createLRUImplementations() {
        return new Object[][]
                {
                        {new CustomIntrusiveListLRUMemCache<Integer, Integer>(10)},
                        {new LinkedHashMapLRUMemCache<Integer, Integer>(10)},
                        {new ConfigurableMemCache<Integer, Integer>(10, ConfigurableMemCache.EvictionStrategy.LRU)}
                };
    }

    @Test(dataProvider = "LRUCaches")
    public void testRecentlyUsedEntriesAreEvicted(MemCache<Integer, Integer> cache) {
        for (int i = 0; i < 10; ++i) {
            cache.put(i, i);
        }

        for (int i = 0; i < 10; i += 2) {
            if ((i / 2) % 2 == 0) cache.get(i);
            else cache.put(i, i);
        }

        for (int i = 10; i < 15; ++i) {
            cache.put(i, i);
        }

        for (int i = 0; i < 10; i += 2) assertTrue(cache.get(i).isPresent());
        for (int i = 1; i < 10; i += 2) assertFalse(cache.get(i).isPresent());
        for (int i = 10; i < 15; ++i) assertTrue(cache.get(i).isPresent());
    }

    @Test(dataProvider = "LRUCaches")
    public void testCorrectValuesAreStored(MemCache<Integer, Integer> cache) {
        CacheTestUtil.testCorrectValuesAreStored(cache);
    }

    @Test(dataProvider = "LRUCaches")
    public void testClearWorks(MemCache<Integer, Integer> cache) {
        CacheTestUtil.testClearWorks(cache);
    }

    @Test(dataProvider = "LRUCaches")
    public void testRemovalWorks(MemCache<Integer, Integer> cache) {
        CacheTestUtil.testRemovalWorks(cache);
    }

    @Test(dataProvider = "LRUCaches")
    public void testNullValuesAreHandledProperly(MemCache<Integer, Integer> cache) {
        CacheTestUtil.testNullValuesAreHandledProperly(cache);
    }
}
