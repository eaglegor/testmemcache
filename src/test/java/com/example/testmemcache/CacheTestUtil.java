package com.example.testmemcache;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class CacheTestUtil {

    public static void testCorrectValuesAreStored(MemCache<Integer, Integer> cache) {
        for (int i = 0; i < 10; i += 2) {
            cache.put(i, i);
        }
        for (int i = 1; i < 10; i += 2) {
            cache.put(i, i);
        }

        for (int i = 0; i < 10; ++i) assertEquals(cache.get(i).get(), Integer.valueOf(i));
    }

    public static void testClearWorks(MemCache<Integer, Integer> cache) {
        for (int i = 0; i < 10; ++i) {
            cache.put(i, i);
        }

        cache.clear();

        for (int i = 0; i < 10; ++i) assertFalse(cache.get(i).isPresent());
    }

    public static void testRemovalWorks(MemCache<Integer, Integer> cache) {
        for (int i = 0; i < 10; ++i) {
            cache.put(i, i);
        }

        for (int i = 0; i < 10; i += 2) {
            cache.remove(i);
        }

        for (int i = 0; i < 10; ++i) assertEquals(cache.get(i).isPresent(), i % 2 != 0);
    }

    public static void testNullValuesAreHandledProperly(MemCache<Integer, Integer> cache) {
        assertFalse(cache.get(null).isPresent());
        cache.remove(null);
    }
}
